module.exports = function(grunt) {

  grunt.initConfig({

    // check all js files for errors
    jshint: {
      all: ['public/dashboardApp/**/*.js'] 
    },

    // process the less file to style.css
    less: {
      build: {
        files: {
          'public/dashboardApp/styles/css/style.css': 'public/dashboardApp/styles/less/style.less'
        }
      }
    },

    // take the processed style.css file and minify
    cssmin: {
      build: {
        files: {
          'public/dashboardApp/styles/css/style.min.css': 'public/dashboardApp/styles/css/style.css'
        }
      }
    },

    // watch css and js files and process the above tasks
    watch: {
      css: {
        files: ['public/dashboardApp/styles/less/*.less'],
        tasks: ['less', 'cssmin']
      },
      js: {
        files: ['public/dashboardApp/**/*.js'],
        tasks: ['jshint']
      }
    },

    // watch our node server for changes
    nodemon: {
      dev: {
        script: 'server.js'
      }
    },

    // run watch and nodemon at the same time
    concurrent: {
      options: {
        logConcurrentOutput: true
      },
      tasks: ['nodemon', 'watch']
    }   

  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  //grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-nodemon');
  grunt.loadNpmTasks('grunt-concurrent');

  grunt.registerTask('default', ['less', 'cssmin', 'jshint', 'concurrent']);

};