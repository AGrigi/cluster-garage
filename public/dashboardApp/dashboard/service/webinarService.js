angular.module('clusterGarage').factory('WebinarService', ['$q', '$http', function ($q, $http) {


	return ({
      postWebinar: postWebinar,
      getAllWebinars: getAllWebinars,
      deleteWebinar: deleteWebinar,
      getWebinarData: getWebinarData,
      updateWebinar: updateWebinar
    });

	function postWebinar(category, name, link, date, description, owner, ownerId) {

      var payLoad = {
            category: category,
            name: name,
            link: link,
            date: date,
            description: description,
            owner: owner,
            ownerId: ownerId
        };

      return $http.post('/webinars', payLoad);

    }

    function getAllWebinars() {
		return $http.get('/webinars');
	}

	function deleteWebinar(id) {
		return $http.delete('/webinars/'+id);
	}

	function getWebinarData(id) {
		return $http.get('/webinars/'+id);
	}

	function updateWebinar(id, category, name, link, date, description) {
		var payLoad = {
            category: category,
            name: name,
            link: link,
            date: date,
            description: description
        };

        return $http.put('/webinars/' + id, JSON.stringify(payLoad));
	}

}]);