angular.module('clusterGarage').factory('UserService', ['$q', '$http', function ($q, $http) {


	return ({
      getCurrentUser: getCurrentUser,
      getUserData: getUserData,
      getAllUsers: getAllUsers,
      deleteUser: deleteUser,
      updatePicture: updatePicture
    });

	function getCurrentUser() { 
	    return $http.get('/logged');
	}

	function getUserData(id) {
		return $http.get('/users/'+id);
	}

	function getAllUsers() {
		return $http.get('/users');
	}

	function deleteUser(id) {
		return $http.delete('/users/'+id);
	}

	function updatePicture(id) {
	
	var payLoad = {
		profilePic: '/pictures/' + id + '.png'
	};
	var deferred = $q.defer();

	$http.put('/users/' + id, JSON.stringify(payLoad))
	// handle success
	.success(function (data) {
		console.log(payLoad); 
		console.log(data);
		deferred.resolve();
	})
	// handle error
	.error(function (data) {
		console.log(data);
		deferred.reject();
	});

	// return promise object
	return deferred.promise;
	}
}]);