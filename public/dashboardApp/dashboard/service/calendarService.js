angular.module('clusterGarage').factory('alert', function($uibModal) {

    function show(action, event) {
      return $uibModal.open({
        templateUrl: '/dashboardApp/dashboard/views/modalCalendarContent.html',
        controller: function() {
          var vm = this;
          vm.action = action;
          vm.event = event;
        },
        controllerAs: 'vm'
      });
    }

    return {
      show: show
    };

  });