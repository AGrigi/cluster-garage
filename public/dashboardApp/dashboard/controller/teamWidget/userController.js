angular.module('clusterGarage').controller('UserController', 
	['$scope', '$route', 'UserService', 'AuthService',
	function($scope, $route, UserService, AuthService) {
	$scope.userlist = [];
	var userId = null;
    UserService.getCurrentUser()
    	.then(function(res) {
			userId = res.data.id;
			UserService.getAllUsers()
				.then(function(res) {
					var names = res.data;
					for (var i=0; i<Object.keys(res.data).length; i++) {
						var userName = [];
						userName.MenuToggle = true;
						if(names[i]._id === userId) {
							userName.ActiveUser = true;
						} else {
							userName.ActiveUser = false;
						}
						userName.id = names[i]._id;
						userName.pos = i;
						userName.name = names[i].firstName + ' ' + names[i].lastName;
						userName.pic = names[i].profilePic;
						$scope.userlist.push(userName);
					}
				});
	});
	$scope.callFunction = function (i, name){
        if(angular.isFunction($scope[name]))
           	$scope[name](i);
    };
	$scope.showMenu = function(i){
		var number = parseInt(i);
		$scope.userlist[number].MenuToggle = !$scope.userlist[number].MenuToggle;
	};
	$scope.deleteUser = function(id){

		UserService.deleteUser(id)
			.then(function(resDel) {
				console.log(resDel.data.message);
				$scope.userlist = [];
				UserService.getAllUsers()
					.then(function(res) {
						var names = res.data;
						for (var i=0; i<Object.keys(res.data).length; i++) {
							var userName = [];
							userName.MenuToggle = true;
							if(names[i]._id === userId) {
								userName.ActiveUser = true;
							} else {
								userName.ActiveUser = false;
							}
							userName.id = names[i]._id;
							userName.pos = i;
							userName.name = names[i].firstName + ' ' + names[i].lastName;
							userName.pic = names[i].profilePic;
							$scope.userlist.push(userName);
						}
					});
			});
	};
}])
 .filter('search', function () {
    return function (users, searchString) {
        if (!searchString || !users) return users;
        var out = [];
        for (var userId in users) {
            if (users[userId].name.toLowerCase().indexOf(searchString.toLowerCase()) != -1){
                    out.push(users[userId]);
                }
        }
        return out;
    };
});