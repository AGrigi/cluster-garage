angular.module('clusterGarage').controller('InsertUserCtrl', 
	['$scope', '$uibModal', 'AuthService', '$route', 'UserService', 
	function($scope, $uibModal, AuthService, $route, UserService) {
	$scope.openInsert = function () {
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: '/dashboardApp/dashboard/views/userInsert.html',
			controller: 'ModalInstanceCtrl',
			resolve: {
				items: function () {
					return $scope.message;
				}
			}
		});
		modalInstance.result.then(function () {
			$scope.$parent.userlist = [];
			var userId = null;
		    UserService.getCurrentUser()
		    	.then(function(res) {
					userId = res.data.id;
					UserService.getAllUsers()
						.then(function(res) {
							var names = res.data;
							for (var i=0; i<Object.keys(res.data).length; i++) {
								var userName = [];
								userName.MenuToggle = true;
								if(names[i]._id === userId) {
									userName.ActiveUser = true;
								} else {
									userName.ActiveUser = false;
								}
								userName.id = names[i]._id;
								userName.pos = i;
								userName.name = names[i].firstName + ' ' + names[i].lastName;
								userName.pic = names[i].profilePic;
								$scope.$parent.userlist.push(userName);
							}
							console.info('Modal dismissed at: ' + new Date());
						});
			});			
		});
	};
	$scope.register = function () {

      // initial values
      $scope.error = false;

      // call register from service
      AuthService.register($scope.insertUser.firstName, $scope.insertUser.lastName, $scope.insertUser.email, $scope.insertUser.password, $scope.insertUser.role)
        // handle success
        .then(function () {
          console.log('Inserted ' + $scope.insertUser.firstName + ' ' + $scope.insertUser.lastName + ' in your cluster!');
          $scope.insertUser = {};
          $scope.$parent.ok();
        })
        // handle error
        .catch(function () {
          $scope.error = true;
          $scope.errorMessage = "Something went wrong!";
          $scope.insertUser = {};
          $scope.$parent.ok();
        });

    };
}]);