angular.module('clusterGarage').controller('EditUserCtrl', 
	['$scope', 'Upload', '$uibModal', 'AuthService', '$route', 'UserService', '$timeout',
	function($scope, Upload, $uibModal, AuthService, $route, UserService, $timeout) {
	$scope.openEdit = function (id) {
		$scope.userpic = '/pictures/'+ id + '.png';
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: '/dashboardApp/dashboard/views/userEdit.html',
			controller: 'ModalInstanceCtrl',
			scope: $scope,
			resolve: {
				items: function () {
					return $scope.message;
				}
			}
		});
		UserService.getUserData(id)
			.then(function(res) {
	    		$scope.editForm = {
					'firstName': res.data.firstName,
					'lastName': res.data.lastName,
					'email': res.data.email
				};
  		});
		$scope.updateUser = function (){
			var file = $scope.f;
			console.log('file');
	        AuthService.update(id, $scope.editForm.firstName, $scope.editForm.lastName, $scope.editForm.email, $scope.editForm.password, $scope.editForm.desc)
	        .then(function(){
	        	console.log('Updated ' + $scope.editForm.firstName + ' ' + $scope.editForm.lastName + ' details!');
	        })
	        .catch(function () {
	        	console.log('Did not work!');
	        });
	        if($scope.f !== undefined) {
	        	 if (file) {
		            file.upload = Upload.upload({
		                url: 'http://localhost:3000/users/' + id + '/upload',
		                data: {file: file}
		            });

		            file.upload.then(function (response) {
		                $timeout(function () {
		                	console.log(response);
		                    file.result = response.data;
		                });
		            }, function (response) {
		                if (response.status > 0)
		                    $scope.errorMsg = response.status + ': ' + response.data;
		            });
		        }
	        	UserService.updatePicture(id)
	        	.then(function(){
	        		console.log('User ' + id + ' updated his profile picture!');
	        	})
	        	.catch(function(){
	        		console.log('Something went wrong with the picture update');
	        	});
	        }
	        modalInstance.close();
		};
		 $scope.previewFiles = function(file, errFiles) {
		 	console.log(file);
		 	$scope.tempurl = file.$ngfBlobUrl;
	        $scope.f = file;
	        $scope.errFile = errFiles && errFiles[0];
	        $scope.selectedPicture = true; 
		 	 
	    };
		modalInstance.result.then(function () {
			$scope.$parent.$parent.userlist = [];
			$scope.$parent.$parent.tagline = $scope.editForm.firstName + ' ' + $scope.editForm.lastName;
			var userId = null;
		    UserService.getCurrentUser()
		    	.then(function(resCurrent) {
					userId = resCurrent.data.id;
					UserService.getAllUsers()
						.then(function(res) {
							var names = res.data;
							for (var i=0; i<Object.keys(res.data).length; i++) {
								var userName = [];
								userName.MenuToggle = true;
								userName.name = names[i].firstName + ' ' + names[i].lastName;
								if(names[i]._id === userId) {
									userName.ActiveUser = true;
								} else {
									userName.ActiveUser = false;
								}
								userName.id = names[i]._id;
								userName.pos = i;
								userName.pic = names[i].profilePic;
								$scope.$parent.userlist.push(userName);
							}
							console.info('Modal dismissed at: ' + new Date());
						});
			});			
		});
	};
}]);