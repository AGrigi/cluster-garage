angular.module('clusterGarage').controller('ViewProfileCtrl', 
	['$scope', '$http', '$uibModal', 'UserService', 'AuthService',
	function($scope, $http, $uibModal, UserService, AuthService) {
	$scope.openProfile = function (id) {

		UserService.getUserData(id)
			.then(function(res) {
    		$scope.name = res.data.firstName + ' ' + res.data.lastName;
    		$scope.email = res.data.email;
			$scope.pic = res.data.profilePic;
			$scope.cluster = res.data.clusterName;
			$scope.desc = res.data.description ? res.data.description: 'Currently this user has no description.';
  		});

		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: '/dashboardApp/dashboard/views/viewProfile.html',
			controller: 'ModalInstanceCtrl',
			scope: $scope,
			resolve: {
				items: function () {
					return $scope.message;
				}
			}
		});

		modalInstance.result.then(function () {
			$scope.$parent.$parent.userlist = [];
			var userId = null;
		    UserService.getCurrentUser()
		    	.then(function(res) {
					userId = res.data.id;
					UserService.getAllUsers()
						.then(function(res) {
							var names = res.data;
							for (var i=0; i<Object.keys(res.data).length; i++) {
								var userName = [];
								userName.MenuToggle = true;
								if(names[i]._id === userId) {
									userName.ActiveUser = true;
								} else {
									userName.ActiveUser = false;
								}
								userName.id = names[i]._id;
								userName.pos = i;
								userName.name = names[i].firstName + ' ' + names[i].lastName;
								userName.pic = names[i].profilePic;
								$scope.$parent.userlist.push(userName);
							}
							console.info('Modal dismissed at: ' + new Date());
						});
			});			
		});
	};
}]);