angular.module('clusterGarage').controller('ModalInstanceCtrl', 
  ['$scope', '$uibModalInstance', 'items', 'UserService', 'AuthService',
  function ($scope, $uibModalInstance, items, UserService, AuthService) {

  $scope.ok = function () {
      $uibModalInstance.close();
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
}]);