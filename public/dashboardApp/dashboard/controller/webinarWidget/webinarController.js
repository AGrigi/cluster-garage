angular.module('clusterGarage').controller('WebinarController', 
	['$scope', '$route', 'WebinarService',
	function($scope, $route, WebinarService) {
		$scope.webinarlist = [];
		$scope.ListToggle = true;
		$scope.CalendarToggle = false;
		WebinarService.getAllWebinars()
			.then(function(res){
				var webinars = res.data;
				for (var i=0; i<Object.keys(res.data).length; i++) {
					var someWebinar = [];
					someWebinar.Menu = true;
					someWebinar.pos = i;
					someWebinar.id = webinars[i]._id;
					someWebinar.userId = webinars[i].ownerId;
					someWebinar.name = webinars[i].name;
					someWebinar.owner = webinars[i].owner;
					someWebinar.link = webinars[i].link;
					someWebinar.date = webinars[i].date;
					if(webinars[i].category === 'Database') {
						someWebinar.pic='/pictures/mongodb.png';
					}
					if(webinars[i].category === 'Back-End') {
						someWebinar.pic='/pictures/expressjs.png';
					}
					if(webinars[i].category === 'Front-End') {
						someWebinar.pic='/pictures/angular.jpg';
					}
					if(webinars[i].category === 'Server-Side') {
						someWebinar.pic='/pictures/node.png';
					}
					$scope.webinarlist.push(someWebinar);
				}
			});
			
		$scope.callWebFunction = function (i, name){
	        if(angular.isFunction($scope[name]))
	           	$scope[name](i);
	    };
		$scope.showWebMenu = function(i){
			var number = parseInt(i);
			$scope.webinarlist[number].Menu = !$scope.webinarlist[number].Menu;
		};
		$scope.changeToList = function() {
			$scope.CalendarToggle = false;
			$scope.ListToggle = true;
		};
		$scope.changeToCalendar = function() {
			$scope.ListToggle = false;
			$scope.CalendarToggle = true;
		};
		$scope.deleteWebinar = function(id){

			WebinarService.deleteWebinar(id)
				.then(function(resDel) {
					console.log(resDel.data.message);
					$scope.webinarlist = [];
					WebinarService.getAllWebinars()
						.then(function(res) {
							var webinars = res.data;
							for (var i=0; i<Object.keys(res.data).length; i++) {
								var someWebinar = [];
								someWebinar.Menu = true;
								someWebinar.pos = i;
								someWebinar.id = webinars[i]._id;
								someWebinar.userId = webinars[i].ownerId;
								someWebinar.name = webinars[i].name;
								someWebinar.owner = webinars[i].owner;
								someWebinar.link = webinars[i].link;
								someWebinar.date = webinars[i].date;
								if(webinars[i].category === 'Database') {
									someWebinar.pic='/pictures/mongodb.png';
								}
								if(webinars[i].category === 'Back-End') {
									someWebinar.pic='/pictures/expressjs.png';
								}
								if(webinars[i].category === 'Front-End') {
									someWebinar.pic='/pictures/angular.jpg';
								}
								if(webinars[i].category === 'Server-Side') {
									someWebinar.pic='/pictures/node.png';
								}
								$scope.webinarlist.push(someWebinar);
							}
						});
				});
		};
}]);