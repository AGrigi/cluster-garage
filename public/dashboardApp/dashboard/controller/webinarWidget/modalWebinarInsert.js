angular.module('clusterGarage').controller('InsertWebinarCtrl', 
	['$scope', '$uibModal', '$route', 'UserService', 'WebinarService',
	function($scope, $uibModal, $route, UserService, WebinarService) {
	var user = null;
	UserService.getCurrentUser()
		.then(function(res) {
			user = res.data.fname + ' ' + res.data.lname;
			userId = res.data.id;
		});
	$scope.openInsert = function () {
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: '/dashboardApp/dashboard/views/webinarInsert.html',
			controller: 'ModalInstanceCtrl',
			resolve: {
				items: function () {
					return $scope.message;
				}
			}
		});
		modalInstance.result.then(function () {
			$scope.$parent.webinarlist = [];
			WebinarService.getAllWebinars()
				.then(function(res){
					var webinars = res.data;
					for (var i=0; i<Object.keys(res.data).length; i++) {
						var someWebinar = [];
						someWebinar.Menu = true;
						someWebinar.pos = i;
						someWebinar.id = webinars[i]._id;
						someWebinar.userId = webinars[i].ownerId;
						someWebinar.name = webinars[i].name;
						someWebinar.owner = webinars[i].owner;
						someWebinar.link = webinars[i].link;
						someWebinar.date = webinars[i].date;
						if(webinars[i].category === 'Database') {
							someWebinar.pic='/pictures/mongodb.png';
						}
						if(webinars[i].category === 'Back-End') {
							someWebinar.pic='/pictures/expressjs.png';
						}
						if(webinars[i].category === 'Front-End') {
							someWebinar.pic='/pictures/angular.jpg';
						}
						if(webinars[i].category === 'Server-Side') {
							someWebinar.pic='/pictures/node.png';
						}
						$scope.$parent.webinarlist.push(someWebinar);
					}
					console.info('Modal dismissed at: ' + new Date());
				});
		});
	};

    $scope.isOpen = false;

    $scope.openCalendar = function(e) {
        e.preventDefault();
        e.stopPropagation();

       	$scope.isOpen = true;
    };

	$scope.register = function () {
		WebinarService.postWebinar($scope.insertWebinar.category, $scope.insertWebinar.name, $scope.insertWebinar.link, $scope.insertWebinar.date, $scope.insertWebinar.description, user, userId)
		.then(function (res) {
			console.log('Inserted webinar ' + $scope.insertWebinar.name);
			$scope.insertWebinar = {};
			$scope.$parent.ok();
		})
		.catch(function (res) {
			console.log('Something went wrong with adding a webinar!');
			$scope.insertWebinar = {};
			$scope.$parent.ok();
		});
	};
}]);