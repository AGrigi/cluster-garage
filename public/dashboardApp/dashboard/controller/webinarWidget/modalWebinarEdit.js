angular.module('clusterGarage').controller('EditWebinarCtrl', 
	['$scope', '$uibModal', 'WebinarService',
	function($scope, $uibModal, WebinarService) {
	$scope.editWebinar = function (id) {
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: '/dashboardApp/dashboard/views/webinarEdit.html',
			controller: 'ModalInstanceCtrl',
			scope: $scope,
			resolve: {
				items: function () {
					return $scope.message;
				}
			}
		});
		WebinarService.getWebinarData(id)
			.then(function(res) {
	    		$scope.updateWebinar = {
	    			'category': res.data.category,
					'name': res.data.name,
					'link': res.data.link,
					'date': res.data.date,
					'description': res.data.description
				};
  		});
		$scope.isOpen = false;

	    $scope.openCalendar = function(e) {
	        e.preventDefault();
	        e.stopPropagation();

	       	$scope.isOpen = true;
	    };
	    $scope.update = function (){
	        WebinarService.updateWebinar(id, $scope.updateWebinar.category, $scope.updateWebinar.name, $scope.updateWebinar.link, $scope.updateWebinar.date, $scope.updateWebinar.description)
	        .then(function(){
	        	console.log('Updated ' + $scope.updateWebinar.name + ' details!');
	        	modalInstance.close();
	        })
	        .catch(function () {
	        	console.log('Did not work!');
	        	modalInstance.close();
	    	});
	    };

	    modalInstance.result.then(function () {
			$scope.$parent.$parent.webinarlist = [];
			WebinarService.getAllWebinars()
				.then(function(res){
					var webinars = res.data;
					for (var i=0; i<Object.keys(res.data).length; i++) {
						var someWebinar = [];
						someWebinar.Menu = true;
						someWebinar.pos = i;
						someWebinar.id = webinars[i]._id;
						someWebinar.userId = webinars[i].ownerId;
						someWebinar.name = webinars[i].name;
						someWebinar.owner = webinars[i].owner;
						someWebinar.link = webinars[i].link;
						someWebinar.date = webinars[i].date;
						if(webinars[i].category === 'Database') {
							someWebinar.pic='/pictures/mongodb.png';
						}
						if(webinars[i].category === 'Back-End') {
							someWebinar.pic='/pictures/expressjs.png';
						}
						if(webinars[i].category === 'Front-End') {
							someWebinar.pic='/pictures/angular.jpg';
						}
						if(webinars[i].category === 'Server-Side') {
							someWebinar.pic='/pictures/node.png';
						}
						$scope.$parent.webinarlist.push(someWebinar);
					}
					console.info('Modal dismissed at: ' + new Date());
				});
		});
	};
}]);