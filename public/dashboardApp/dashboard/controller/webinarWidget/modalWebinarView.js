angular.module('clusterGarage').controller('ViewWebinarCtrl', 
	['$scope', '$http', '$uibModal', 'WebinarService',
	function($scope, $http, $uibModal, WebinarService) {
		$scope.openWebinar = function (id) {

		WebinarService.getWebinarData(id)
			.then(function(res) {
    		$scope.name = res.data.name;
    		$scope.link = res.data.link;
    		$scope.date = res.data.date;
			$scope.category = res.data.category;
			if($scope.category === 'Database') {
				$scope.pic='/pictures/mongodb.png';
			}
			if($scope.category === 'Back-End') {
				$scope.pic='/pictures/expressjs.png';
			}
			if($scope.category === 'Front-End') {
				$scope.pic='/pictures/angular.jpg';
			}
			if($scope.category === 'Server-Side') {
				$scope.pic='/pictures/node.png';
			}
			$scope.desc = res.data.description ? res.data.description : 'Currently this webinar has no description.';
			$scope.users = res.data.userList ? res.data.userList : 'Currently this webinar has no members.';
  		});

		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: '/dashboardApp/dashboard/views/viewWebinar.html',
			controller: 'ModalInstanceCtrl',
			scope: $scope,
			resolve: {
				items: function () {
					return $scope.message;
				}
			}
		});

		modalInstance.result.then(function () {

		});
	};
}]);