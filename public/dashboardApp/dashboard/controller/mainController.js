angular.module('clusterGarage').controller('MainController',
	['$scope', 'AuthService', 'Idle', '$location', 'UserService',
	function($scope, AuthService, Idle, $location, UserService) {

		UserService.getCurrentUser().then(function(res) {
			$scope.tagline = res.data.fname + ' ' + res.data.lname;
		});

		$scope.$on('IdleTimeout', function() {
	        $scope.logout();
    	});

    	$scope.logout = function(){
			AuthService.logout()
	        .then(function () {
	          $location.path('/login');
	        });
	        console.log('logged out');
		};

}]);
/*.run(function(Idle){
    Idle.watch();
});*/