 angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

    $routeProvider
        .when('/', {
            templateUrl: 'dashboardApp/dashboard/views/main.html',
            controller: 'MainController',
            access: {restricted: true}
        })
        .when('/login', {
        	templateUrl: 'dashboardApp/auth/views/login.html',
            controller: 'LoginController',
            access: {restricted: false}
        })
        .when('/logout', {
  			controller: 'LogoutController',
  			access: {restricted: true}
		})
        .when('/register', {
        	templateUrl: 'dashboardApp/auth/views/register.html',
            controller: 'RegisterController',
            access: {restricted: false}
        })
        .otherwise({
      		redirectTo: '/'
    	});

    $locationProvider.html5Mode({
    	enabled: true,
    	requireBase: false
    });

}]);

angular.module('clusterGarage').run(function ($rootScope, $location, $route, AuthService) {
	$rootScope.$on('$routeChangeStart', function (event, next, current) {
		AuthService.getStatus()
		.then(function(){
			if (next.access.restricted && !AuthService.isLoggedIn()){
				$location.path('/login');
				$route.reload();
			}
            else if(next.templateUrl === 'dashboardApp/auth/views/login.html' && AuthService.isLoggedIn()) {
                $location.path('/');
                $route.reload();
            }
		});
	});
});