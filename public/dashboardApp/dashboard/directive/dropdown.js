angular.module('clusterGarage').directive('bsDropdown', function ($compile, $parse) {
    return {
        restrict: 'E',
        scope: {
            user: '=info',
            items: '=dropdownData',
            doSelect: '&selectVal',
        },
        link: function (scope, element, attrs) {
            var html = '';
            html += '<div ng-controller="MainController as main" class="btn-group"><button class="btn button-label btn-info">{{user}}</button><button class="btn btn-info dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>';
            html += '<ul class="dropdown-menu"><li ng-click="main.' + scope.items[0].command + '"><a tabindex="0">' + scope.items[0].name + '</a></li>';
            html += '<li ng-click="main.' + scope.items[1].command + '"><a tabindex="1">' + scope.items[1].name + '</a></li></ul></div>';
            element.append($compile(html)(scope));
        }
    };
});