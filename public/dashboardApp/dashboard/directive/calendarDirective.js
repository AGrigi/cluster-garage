angular.module('clusterGarage')
    .directive('calendarDir', function(){
      return {
        restrict: 'E',
        scope: false,
        replace: true,
        templateUrl: 'dashboardApp/dashboard/views/calendar.html'
      };
    });