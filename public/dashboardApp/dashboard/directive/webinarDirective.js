angular.module('clusterGarage')
    .directive('webinarList', function(){
      return {
        restrict: 'E',
        scope: false,
        replace: true,
        templateUrl: 'dashboardApp/dashboard/views/webinars.html'
      };
    });