angular.module('clusterGarage')
    .directive('userList', function(){
      return {
        restrict: 'E',
        scope: false,
        replace: true,
        templateUrl: 'dashboardApp/dashboard/views/users.html'
      };
    });