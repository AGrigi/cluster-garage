angular.module('idle', ['ngIdle']).config(function(IdleProvider, KeepaliveProvider) {
  IdleProvider.idle(5);
  IdleProvider.timeout(1); 
  KeepaliveProvider.interval(5);
});