angular.module('clusterGarage').factory('AuthService', ['$q', '$http', function ($q, $http) {

    // create user variable
    var userLogged = null;
    var currentUser = '';

    // return available functions for use in the controllers
    return ({
      isLoggedIn: isLoggedIn,
      login: login,
      logout: logout,
      register: register,
      update: update,
      getStatus: getStatus,
      getUser: getUser,
      getId: getId
    });

    function isLoggedIn() {
      if(userLogged) {
        return true;
      } else {
        return false;
      }
           
    }

    function login(email, password) {

    var payLoad = {
        email: email.trim(),
        password: password.trim()
    };

    var deferred = $q.defer();

      // send a post request to the server
      $http.post('/login', payLoad)
        // handle success
        .success(function (data) {
            userLogged = true;
            currentUser = data.user;
            deferred.resolve();
        })
        // handle error
        .error(function (err) {
          console.log(err);
          userLogged = false;
          deferred.reject();
        });

      // return promise object
      return deferred.promise;

    }

    function logout() {

      // create a new instance of deferred
      var deferred = $q.defer();

      // send a get request to the server
      $http.get('/logout')
        // handle success
        .success(function (data) {
          userLogged = false;
          currentUser = '';
          deferred.resolve();
        })
        // handle error
        .error(function (data) {
          userLogged = false;
          deferred.reject();
        });

      // return promise object
      return deferred.promise;

    }

    function register(firstName, lastName, email, password, role) {

      var payLoad = {
            firstName: firstName.trim(),
            lastName: lastName.trim(),
            email: email.trim(),
            password: password.trim(),
            role: role.trim()
        };

        // create a new instance of deferred
      var deferred = $q.defer();

      // send a post request to the server
      $http.post('/users', payLoad)
        // handle success
        .success(function (data) {
            deferred.resolve();
        })
        // handle error
        .error(function (err) {
          console.log(err);
          deferred.reject();
        });

      // return promise object
      return deferred.promise;
    }

    function update(id, firstName, lastName, email, password, description) {
      var payLoad = null;
      if(password === undefined && description !== undefined) {
        payLoad = {
            firstName: firstName.trim(),
            lastName: lastName.trim(),
            email: email.trim(),
            description: description.trim()
        };
      } else if (password !== undefined && description === undefined) {
         payLoad = {
            firstName: firstName.trim(),
            lastName: lastName.trim(),
            email: email.trim(),
            password: password.trim()
        };
      } else if (password === undefined && description === undefined) {
        payLoad = {
            firstName: firstName.trim(),
            lastName: lastName.trim(),
            email: email.trim()
        };
      } else if (password !== undefined && description !== undefined) {
        payLoad = {
            firstName: firstName.trim(),
            lastName: lastName.trim(),
            email: email.trim(),
            password: password.trim(),
            description: description.trim()
        };
      }

        // create a new instance of deferred
      var deferred = $q.defer();

      $http.put('/users/' + id, JSON.stringify(payLoad))
        // handle success
        .success(function (data) {
            deferred.resolve();
        })
        // handle error
        .error(function (err) {
          console.log(err);
          deferred.reject();
        });

      // return promise object
      return deferred.promise;
    }

    function getStatus() {
        return $http.get('/logged')
        // handle success
        .success(function (data) {
          if(data.authenticated){
            userLogged = true;
          } else {
            userLogged = false;
          }
        })
        // handle error
        .error(function (data) {
          userLogged = false;
        });
      }

      function getUser() {
        var userName = currentUser.firstName + ' ' + currentUser.lastName;
        return userName;
      }

      function getId() {
        var userId = currentUser._id;
        return userId;
      }

}]);