angular.module('clusterGarage').controller('LoginController',
  ['$scope', '$location', 'AuthService', 'UserService',
  function ($scope, $location, AuthService, UserService) {

    $scope.go = function ( path ) {
      $location.path( path );
    };

    $scope.login = function () {

      // initial values
      $scope.error = false;

      // call login from service
      AuthService.login($scope.loginForm.email, $scope.loginForm.password)
         // handle success
        .then(function () {
          $location.path('/');
          $scope.loginForm = {};
        })
        // handle error
        .catch(function () {
          $scope.error = true;
          $scope.errorMessage = "Invalid username and/or password";
          $scope.loginForm = {};
        });

    };

}]);