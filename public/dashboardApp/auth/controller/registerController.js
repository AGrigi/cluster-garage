angular.module('clusterGarage').controller('RegisterController',
  ['$scope', '$location', 'AuthService',
  function ($scope, $location, AuthService) {

    $scope.go = function ( path ) {
      $location.path( path );
    };

    $scope.register = function () {

      // initial values
      var roleBasic = 'Basic';
      $scope.error = false;

      // call register from service
      AuthService.register($scope.registerForm.firstName, $scope.registerForm.lastName, $scope.registerForm.email, $scope.registerForm.password, roleBasic)
        // handle success
        .then(function () {
          $location.path('/login');
          $scope.registerForm = {};
        })
        // handle error
        .catch(function () {
          $scope.error = true;
          $scope.errorMessage = "Something went wrong!";
          $scope.registerForm = {};
        });

    };

}]);