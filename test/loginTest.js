var newUser, data;

var should   = require("should");
var app      = require("../server");
var mongoose = require("mongoose");
var User     = mongoose.model("User");
var request  = require("supertest");
var agent = request.agent(app);

describe('User', function () {
  before(function(done) {
      newUser = new User({
        firstName: "Amziscaerapopa",
        lastName : "Dartubatmanbatman",
        email    : "batman@dorel.ro",
        password : "nuerapopa"
      });

      newUser.password = newUser.hashPassword(newUser.password);

      newUser.save(function(err){
        if(err){
          console.log(err);
          return res.send(err);
        }

        return done();
      });
    });
  describe('Login test', function () {
      data = JSON.stringify({ 
        'email': 'batman@dorel.ro', 
        'password': 'nuerapopa' 
      });
      
      it('should log', function (done) {
        agent
        .post('/login')
        .type('json')
        .send(data)
        .end(done);
      });

    after(function(done) {
          User.find({ 'email': 'batman@dorel.ro' }).remove().exec();
          return done();
      });

  });
});