var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var librarySchema = new Schema({
	name: {
		type: String,
		required: true
	},
	category: {
		type: String,
		required: true
	},
	resourceType: {
		type: String,
		required: true,
		enum: ['link', 'file']
	},
	link: {
		type: String,
		required: true
	},
	fileUpload: {
		data: Buffer,
    	contentType: String
	}
});

module.exports = mongoose.model('Library', librarySchema);