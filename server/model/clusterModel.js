var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var clusterSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  teamMembersList: [{
    member: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    }
  }],
  resourceList[{
    library: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Library'
    }
  }],
  upcomingWebinarsList: [{
    webinar: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Webinar'
    }
  }]
});

module.exports = mongoose.model('Cluster', clusterSchema);