var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var webinarSchema = new Schema({
	name: {
		type: String,
		required: true
	},
	link: {
		type: String,
		required: true
	},
	date: {
		type: Date,
		required: true
	},
	category: {
		type: String,
		required: true
	},
	cluster: {
		type: String,
		default: null
	},
	owner: {
		type: String,
		required: true
	},
	ownerId: {
		type: String,
		require: true
	},
	description: {
		type: String,
		default: null
	}
});

module.exports = mongoose.model('Webinar', webinarSchema);