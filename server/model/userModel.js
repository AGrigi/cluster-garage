var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var Schema = mongoose.Schema;

var userSchema = new Schema({
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true,
    match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
  },
  password: {
    type: String,
    required: true
  },
  clusterName: {
    type: String,
    default: null
  },
  description: {
    type: String,
    default: null
  },
  profilePic: {
    type: String,
    default: "/pictures/default-avatar.png"
  },
  role: {
    type: String,
    enum: ["Basic", "Admin"],
    default: "Basic"
  },
  webinarJoinedList: [{
    name: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Webinar'
    }
  }]
});

userSchema.methods.hashPassword = function (password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);
};

userSchema.methods.checkPassword = function (password) {
  return bcrypt.compareSync(password, this.password);
}
userSchema.methods.randomPasswordHashed = function () {
  return this.hashPassword(Math.random().toString(36).substr(2, 8));
}

module.exports = mongoose.model('User', userSchema);
