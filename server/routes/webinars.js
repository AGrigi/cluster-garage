var express = require('express');
var router = express.Router();
var Webinar = require('../model/webinarModel');

router.route('/')
  //Get all webs
  .get(function (req, res) {

    Webinar.find(function (err, webs) {
      if (err) {
        return res.send(err);
      }
      res.json(webs);
    });

  })
  //Add a new web to the collection
  .post(function (req, res) {
    var web = new Webinar(req.body);

    web.save(function (err, web) {
      if (err) {
        return res.send(err);
      }
      return res.send('Successfuly added webinar ' + web.id + '.');
    });
  });

  router.route('/:id')
  //Get a single web
  .get(function (req, res) {
    Webinar.findOne({_id: req.params.id}, function (err, web) {
      if (err) {
        return res.send(err);
      }
      res.json(web);
    });
  })
  //Update an existing web
  .put(function (req, res) {
    Webinar.findOne({_id: req.params.id}, function (err, web) {
      if (err) {
        return res.send(err);
      }
      //Update the web with the received fields
      for (p in req.body) {
        web[p] = req.body[p];
      }
      //save the web
      web.save(function (err) {
        if (err) {
          return res.send(err);
        }
        res.json({message: 'Webinar ' + req.params.id + ' successfuly updated'});
      });

    });
  })
  //Remove a web
  .delete(function (req, res) {
    Webinar.remove({_id: req.params.id}, function (err) {
      if (err) {
        return res.send(err);
      }
      res.json({message: 'Webinar ' + req.params.id + ' deleted'});
    });
  });

module.exports = router;