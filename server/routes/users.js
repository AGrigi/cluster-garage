var express = require('express');
var router = express.Router();
var User = require('../model/userModel');
var formidable = require('formidable');
var fs   = require('fs-extra');


router.route('/')
  //Get all users
  .get(function (req, res) {

    User.find(function (err, users) {
      if (err) {
        return res.send(err);
      }
      res.json(users);
    });

  })
  //Add a new user to the collection
  .post(function (req, res) {
    var user = new User(req.body);
    user.password = user.hashPassword(req.body.password);

    user.save(function (err, user) {
      if (err) {
        return res.send(err);
      }
      return res.send('Successfuly added user ' + user.id + '.');
    });
  });

  router.route('/:id')
  //Get a single User
  .get(function (req, res) {
    User.findOne({_id: req.params.id}, {password: 0}, function (err, user) {
      if (err) {
        return res.send(err);
      }
      res.json(user);
    });
  })
  //Update an existing user
  .put(function (req, res) {
    User.findOne({_id: req.params.id}, function (err, user) {
      if (err) {
        return res.send(err);
      }
      //Update the user with the received fields
      for (p in req.body) {
        user[p] = req.body[p];
      }
      if(req.body.password !== undefined) {
      	user.password = user.hashPassword(req.body.password);
      }

      //save the user
      user.save(function (err) {
        if (err) {
          return res.send(err);
        }
        res.json({message: 'User ' + req.params.id + ' successfuly updated'});
      });

    });
  })
  //Remove a user
  .delete(function (req, res) {
    User.remove({_id: req.params.id}, function (err) {
      if (err) {
        return res.send(err);
      }
      res.json({message: 'User ' + req.params.id + ' deleted'});
    });
  });

  router.route('/:id/upload')
	.post( function(req, res) {
		var form = new formidable.IncomingForm();
		form.parse(req, function(err, fields, files) {
			//console.log(files);
		});
		form.on('end', function(fields, files) {
			/* Temporary location of our uploaded file */
			var temp_path = this.openedFiles[0].path;
			/* The file name of the uploaded file */
			var file_name = this.openedFiles[0].name;
			var file_length = file_name.length;
			var extension = file_name.substring(file_length-4, file_length);
			var new_name = req.params.id + extension;
			/* Location where we want to copy the uploaded file */
			var new_location = 'public/pictures/';

			fs.copy(temp_path, new_location + new_name, function(err) {  
			  if (err) {
			    res.send(err);
			  } else {
			    res.send("success!")
			  }
			});
		});
	});

module.exports = router;