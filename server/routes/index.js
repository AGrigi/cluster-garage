var passport = require('passport');
var express = require('express');
var router = express.Router();
var users = require('./users');
var webs = require('./webinars');

module.exports = function(app) {

	router.route('/login')
    .post(passport.authenticate('local-login'),
      function (req, res) {
        res.send({
          authenticated : req.isAuthenticated(),
          user: req.user ? req.user : null
        });
      });

    router.route('/logout')
    .get(function (req, res) {
      req.logout();
      res.end();
    });

    router.route('/logged')
    .get(function (req, res) {
      res.send({
        authenticated : req.isAuthenticated(),
        fname: req.user ? req.user.firstName : null,
        lname: req.user ? req.user.lastName : null,
        id: req.user ? req.user._id : null
      });
    })

    router.use('/users', users);
    router.use('/webinars', webs);

	router.route('*')
    .get(function (req, res) {
      res.render('index.html');
    });

	app.use('/', router);
};

function isAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  res.status(401);
  res.send({message: 'Not logged in.'})
}