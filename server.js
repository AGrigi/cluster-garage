var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var engines = require('consolidate');
var multer = require('multer');

var dbConfig = require('./server/config/db');
var app = express();

var upload = multer({ dest: 'uploads/' });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

require('./server/config/passport')(app);

app.set('views', __dirname + '/public/dashboardApp');
app.engine('html', engines.mustache);
app.set('view engine', 'html');

mongoose.connect(dbConfig.url, function (err) {
  if (err) {
    console.error(err);
    return;
  }
  console.info('Connected to DB.');
});

app.use(express.static(__dirname + '/public')); 

require('./server/routes')(app);


var server = app.listen(process.env.PORT || 3000, function () {
  console.info('\nServer ready on port %d\n', server.address().port);
});

module.exports = app;